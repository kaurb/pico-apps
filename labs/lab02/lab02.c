#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

float wallis_algo(int num)
{
   float val,temp = 1.0;
   for(float i= 1.0 ; i <= num; i++)
   {
       temp *= (((2 * i)/ ((2 * i) - 1)) * ((2 * i)/ ((2 * i) + 1)));
   }
   val = 2 * temp;
   return val;
} 

double wallisalgo(int num)
{
   double val,temp= 1.0; 
   for(double i= 1.0 ; i <= num; i++)
   {
       temp*= (((2 * i)/ ((2 * i) - 1)) * ((2 * i)/ ((2 * i) + 1)));
   }
   val = 2 * temp;
   return val;
} 

int main() {
    int num = 100000;
    double comp = 3.14159265359;

    double val1 = wallisalgo(num);
    printf("Value of pi in double precision floating point representation = %lf\n",val1);
   
    float val2 = wallis_algo(num);
    printf("Value of pi in single precision floating point representation = %f\n",val2);
     double err1 = (((comp - val1)/ comp) * 100);
    printf("Approximation error = %lf\n", err1);
    float err2 = (((comp - val2)/ comp) * 100);
    printf("Approximation error = %f\n", err2);
    return 0;
}
