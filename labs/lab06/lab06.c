#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
#include "pico/time.h" // required for time thingies 


// Must declare the main assembly entry point before use.
void main_asm();

/**
 * @brief LAB #06 - TEMPLATE
 *        Main entry point for the code - calls the main assembly
 *        function where the body of the code is implemented.
 * 
 * @return int      Returns exit-status zero on completion.
 */

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

/**
 * @brief WALLIS PRODUCT, DOUBLE 
 *        Uses the Wallis product to caluclate pi as a double
 *        var. 
 * 
 * @return double  Returns the calculated value of pi
 */

double pi_double (int iter){ 
    double pi_d = 0;
    double theor = 3.14159269359;

    pi_d = 2.0 * 2.0 / 3.0 ; 
    // init pi_d outside for loop, otherwise would produce 0 

    for (int i = 2; i < iter; i++){
        double x = 4.0 * i * i;
        pi_d *= x / (x - 1.0);
    }
    pi_d *= 2; // equation provides pi/2, must compensate. 

    printf("This is the value of double: %f%% \n", pi_d); 
    double d_error = (pi_d - theor) / theor * 100;
    // d_error = abs(d_error); 
    printf("The percent error for double is, %f%% \n", d_error); 

    return pi_d; 
}

/**
 * @brief WALLIS PRODUCT, DOUBLE 
 *        Uses the Wallis product to caluclate pi as a float
 *        var. 
 * 
 * @return float  Returns the calculated value of pi
 */

float pi_float (int iter){
    float pi_f = 2.0f * 2.0f / 3.0f;
    // init pi_f outside for loop, otherwise would produce 0 

    for (int i = 2; i < iter; i++){
        float x = 4.0f * i * i;
        pi_f *= x / (x - 1.0f);
    }

    pi_f *= 2.0f; // equation gives pi/2, must compensate.

    return pi_f; 
}

/** @brief LAB02 PI PICO 
 *        Uses the Wallis product to caluclate pi in two ways. 
 *        Compares the percent error to accepted value for the
 *        the float and double values respectfully. Prints result.
 * 
 * @return int  Application return code (zero for success).
 */

// Main code entry point for core0.
int main() {
    absolute_time_t start_timet = get_absolute_time();
    const int    ITER_MAX   = 100000;

    stdio_init_all();
    multicore_launch_core1(core1_entry);

    // Code for sequential run goes hereâ€¦

    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation
    //    Run the double-precision Wallis approximation
    //    Take snapshot of timer and store
    //    Display time taken for application to run in sequential mode

    // accepted value of pi for error calc
    double theor = 3.14159269359;

    // calls double func, print to terminal
    absolute_time_t start_timed = get_absolute_time();	
    double p_d = pi_double(ITER_MAX);
    
    absolute_time_t end_timed = get_absolute_time();
    int64_t runtimed = absolute_time_diff_us(start_timed, end_timed);
    printf("The individual runtime for double is, %lld \n", runtimed);
    
    
    absolute_time_t start_timef = get_absolute_time(); // get current time, start of the float commands
    float p_f = pi_float(ITER_MAX); // calls float func, print to terminal
    printf("The value for float is, %f \n", p_f); 
    // error calc, abs value taken
    float f_error = (p_f - theor) / theor * 100 ;
    // f_error = abs(f_error); 
    printf("The percent error for float is, %f%% \n", f_error); 

    absolute_time_t end_timef = get_absolute_time();
    int64_t runtimef = absolute_time_diff_us(start_timef, end_timef); //get difference of start and end time of float actions
    printf("The individual runtime for float is, %lld \n", runtimef);

    absolute_time_t end_timet = get_absolute_time();
    int64_t runtime_t = absolute_time_diff_us(start_timet, end_timet); // get total time to run, on one CPU
    printf("The total runtime while running on a single CPU is, %lld \n", runtime_t); 

    // Code for parallel run goes hereâ€¦
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation on one core
    //    Run the double-precision Wallis approximation on the other core
    //    Take snapshot of timer and store
    //    Display time taken for application to run in parallel mode

    absolute_time_t start_timef_tc = get_absolute_time();	
    multicore_fifo_push_blocking((uintptr_t) &pi_double);
    multicore_fifo_push_blocking(ITER_MAX);

    // We could now do a load of stuff on core 0 and get our result later

    //double p_d_tc = pi_double(ITER_MAX); // calls double func, print to terminal

    // calls float func, print to terminal
    absolute_time_t start_timet_tc = get_absolute_time();
    float p_f_tc = pi_float(ITER_MAX); 
    printf("The value for float is, %f \n", p_f_tc); 
    // error calc, abs value taken
    float f_error_tc = (p_f_tc - theor) / theor * 100 ;
    // f_error_tc = abs(f_error_tc); 
    printf("The percent error for float is, %f%% \n", f_error_tc); 

    double p_d_tc = multicore_fifo_pop_blocking();

    absolute_time_t end_timef_tc = get_absolute_time();
    int64_t runtimef_tc = absolute_time_diff_us(start_timef_tc, end_timef_tc);
    printf("The individual runtime for double is, %lld \n", runtimef_tc);

    absolute_time_t end_timet_tc = get_absolute_time();
    int64_t runtime_t_tc = absolute_time_diff_us(start_timet_tc, end_timet_tc);
    printf("The total runtime while running on a dual CPU is, %lld \n", runtime_t_tc);

    

    return 0; // Returning zero indicates everything went okay.
}
